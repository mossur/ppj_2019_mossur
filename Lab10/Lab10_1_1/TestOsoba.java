import pl.imiujd.mossur.*;

import java.util.*;
import java.time.LocalDate;
import java.util.ArrayList;
public class TestOsoba
{
    public static void main(String[] args)
    {
        ArrayList<Osoba> grupa = new ArrayList<Osoba>();
        grupa.add(new Osoba("Tetiana", LocalDate.of(1999, 12, 22)));
        grupa.add(new Osoba("Nadia", LocalDate.of(1999, 09, 29)));
        grupa.add(new Osoba("Diana", LocalDate.of(2000, 05, 26)));
        grupa.add(new Osoba("Sasha", LocalDate.of(1999, 11, 26)));
        grupa.add(new Osoba("Denis", LocalDate.of(1999, 05, 09)));
        for (Osoba p : grupa) {
            System.out.println(p);
        }
        Collections.sort(grupa);
        System.out.println("After sort");
        for (Osoba p : grupa) {
            System.out.println(p);
        }
    }
}

