import java.util.*;
import java.time.LocalDate;


public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Jan", "Kowalski", 50000, LocalDate.of(1990, 10, 20),LocalDate.of(1970, 10, 20),true );
        ludzie[1] = new Student("Małgorzata","Nowak","informatyka",LocalDate.of(1990, 10, 20),96.2,false);

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko()+p.getImie()+": " + p.getOpis()+"; plec: "+p.getPlec());
        }
    }
}

abstract class Osoba
{
    public Osoba(String nazwisko, String imie, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
        
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String getImie()
    {
        return imie;
    }
    public LocalDate getBirth()
    {
        return dataUrodzenia;
    }
    public boolean getPlec()
    {
        return plec;
    }

    private String nazwisko;
    private String imie;
    private LocalDate dataUrodzenia;
    private boolean plec;
}

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String imie, double pobory, LocalDate dataZatrudnienia,LocalDate dataUrodzenia, boolean plec)
    {
        super(nazwisko,imie,dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }
    public LocalDate getDataZatrudnienia()
    {
        return dataZatrudnienia;
    }
    private double pobory;
    private LocalDate dataZatrudnienia;
}


class Student extends Osoba
{
    public Student(String nazwisko, String imie, String kierunek,LocalDate dataUrodzenia, double sredniaOcen, boolean plec)
    {
        super(nazwisko,imie,dataUrodzenia,plec);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek + "; sr.ocen: "+sredniaOcen;
    }
    public double getOcen()
    {
        return sredniaOcen;
    }
    public void setOcen(double sredniaOcen)
    {
        this.sredniaOcen=sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}

