package pl.imiujd.mossur;


import java.time.LocalDate;


public abstract class Osoba
{
    public Osoba(String nazwisko, String imie, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
        
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String getImie()
    {
        return imie;
    }
    public LocalDate getBirth()
    {
        return dataUrodzenia;
    }
    public boolean getPlec()
    {
        return plec;
    }

    private String nazwisko;
    private String imie;
    private LocalDate dataUrodzenia;
    private boolean plec;
}



