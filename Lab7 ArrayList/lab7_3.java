import java.util.ArrayList;

public class lab7_3
{
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> c = new ArrayList<Integer>();
        int i=0, k=0, j=0;
        while(i<a.size() && j<b.size())
            c.add(a.get(i)<b.get(j)?a.get(i++):b.get(j++));
        while(i<a.size())
            c.add(a.get(i++));
        while(j<b.size())
            c.add(b.get(j++));
        return c;
    }

	public static void main(String[] args) 
	{
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1); a.add(2); a.add(3); a.add(100); a.add(200);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(1); b.add(8); b.add(9); b.add(10); b.add(11);
        System.out.println(mergeSorted(b,a));

    }
}

