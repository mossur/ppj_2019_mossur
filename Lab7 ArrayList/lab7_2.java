import java.util.ArrayList;

public class lab7_2
{
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        int l1 = a.size();
        int l2 = b.size();
        int max=l1, min=l2;
        if(l2>l1)
        {
            max=l2;
            min=l1;
        }
        ArrayList<Integer> c = new ArrayList<Integer>();
        for(int i = 0; i<min; i++)
        {
            c.add(a.get(i));
            c.add(b.get(i));
        }
        if(l2==max)
        for(int i = min; i<max; i++)
        {
            c.add(b.get(i));
        }
        else if(l1==max)
        for(int i = min; i<max; i++)
        {
            c.add(a.get(i));
        }

    return c;
    }

	public static void main(String[] args) 
	{
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1); a.add(3); a.add(5); a.add(23); a.add(100);
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(2); b.add(6); b.add(8); b.add(9);
        System.out.println(merge(a,b));

    }
}

