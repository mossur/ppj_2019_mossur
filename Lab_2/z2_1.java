import java.util.Scanner;
import java.lang.Math;

public class z2_1 
{
	public static float factorial(int n)
	{
		if(n <= 2)
		{
			return n;
		}
		return n * factorial(n - 1);
	}
	
	public static void main(String[] args) 
	{
		Scanner in = new Scanner(System.in);
		System.out.println("Input amount of numbers: ");
		int n = in.nextInt();
		
		int a = 0;
		int b = 0;
		int c = 0;
		int d = 0;
		int e = 0;
		int f = 0;
		int g = 0;
		int h = 0;
		float tmp;
		float t1 = 0, t2 = 0;
		System.out.println("Input numbers: ");
		
		for(int i = 0; i < n; i++)
		{
			tmp = in.nextInt();
			if(tmp%2!=0) a++;
			if(tmp%3==0 && tmp%5!=0) b++;
			if(Math.sqrt(tmp)%2==0) c++;
			if(i==0) t1 = tmp;
			if(i==1) t2 = tmp;
			if(i > 1)
			{
				if(t2 <(t1 + tmp)/2) d++;
				t1 = t2;
				t2 = tmp;
			}
			if(Math.pow(2, i+1) < tmp && tmp < factorial(i+1)) e++;
			if((i + 1)%2!=0 && tmp%2==0) f++;
			if(tmp%2!=0 && tmp > 0)g++;
			if(Math.abs(tmp) < (i+1)*(i+1)) h++;
	    }
	    System.out.println("a)" + a);
		System.out.println("b)" + b);
		System.out.println("c)" + c);
		System.out.println("d)" + d);
		System.out.println("e)" + e);
		System.out.println("f)" + f);
		System.out.println("g)" + g);
		System.out.println("h)" + h);
		
	
	}
	
}
