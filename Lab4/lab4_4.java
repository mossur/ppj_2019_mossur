import java.util.Scanner;
import java.lang.Math; 
import java.math.BigInteger;


public class lab4_4
{
	public static void main(String[] args) 
	{
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		n = n*n;
		BigInteger F = new BigInteger("1");
		BigInteger S = new BigInteger("0");
		for(int i = 0; i<n; i++)
		{
            S = S.add(F);
			F = F.multiply(BigInteger.valueOf(2));
		}
		System.out.println(S);
	}
}
