import java.util.*;
public class MapTest
{
    public static void main(String[] args)
    {
        System.out.println("Korzystaj polecen show/add/rem");
        Map<String, String> Mapa = new HashMap<String, String>();
        Mapa.put("Mossur", "bdb");
        Scanner in = new Scanner(System.in);
        while(true)
        {
            String cmd = in.nextLine();
            if(cmd.equals("show")) show(Mapa);
            if(cmd.equals("add")) add(Mapa);
            if(cmd.equals("rem")) rem(Mapa);
        }
    }
    static void show(Map<String, String> Mapa)
    {
        for (Map.Entry<String, String> wpis : Mapa.entrySet()) 
        {
            String key = wpis.getKey();
            String value = wpis.getValue();
            System.out.println(key + ": " + value);
        }
    }
    static void add(Map<String, String> Mapa)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Nazwisko? ");
        String naz = in.next();
        System.out.println("Ocena? ");
        String oc = in.next();
        Mapa.put(naz, oc);
        System.out.println("Dodano");
    }
    static void rem(Map<String, String> Mapa)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Nazwisko? ");
        String naz = in.next();
        Mapa.remove(naz);
        System.out.println("Usunieto");
    }
}
